<?php
class ConectorPump {
	public $version = "0.0.20151219151000";
	public $cliente = null;
	public $metodoCod = null;
	public $usuario = null;
	public $alias = null;
	public $nodo = null;
	public $conexion = null;
	private $consumer_key = null;
	private $consumer_secret = null;
	private $token = null;
	private $token_secret = null;
	private $nvlreg = null;
	public $registro = array();
	public $rutas = array(
		'tt' => '/oauth/request_token',
		'ta' => '/oauth/access_token',
		'rc' => '/api/client/register',
		'aut' => '/oauth/authorize?oauth_token=',
		'whoami' => '/api/whoami',
		'dialback' => '/api/dialback'
	);
	function __construct(array $cli, $nvlreg, $cod = 'HMAC-SHA1') {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if(is_null($cli)) {
			return false;
		}
		$this->cliente = $cli;
		if(!is_null($cod)) {
			$this->metodoCod = $cod;
		}
		$this->nvlreg = $nvlreg;
	}
	function nodo($nodo, $consumer_key, $consumer_secret, $conexion) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if(is_null($nodo) || is_null($consumer_key) || is_null($consumer_secret) || is_null($conexion)) {
			return false;
		}
		$this->nodo = $nodo;
		$this->consumer_key = $consumer_key;
		$this->consumer_secret = $consumer_secret;
		$this->conexion = $conexion;
	}
	function usuario($usuario, $token, $token_secret) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if(is_null($usuario) && is_null($token) && is_null($token_secret)) {
			return false;
		}
		#Verificaciones inecesarias, al principio se verifica si esta vacia.
		if(!is_null($usuario)) {
			$this->usuario = $usuario;
			$this->alias = $this->pumpId($usuario, 'alias');
		}
		if(!is_null($token) && !is_null($token_secret)) {
			$this->token = $token;
			$this->token_secret = $token_secret;
		}
		
	}
	private function rutasGet($id, ...$cyp) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$llaves = ['count', 'since', 'before'];
		$parametros = array();
		foreach($cyp as $k => $v) {
			$parametros[] = $llaves[$k].'='.$v;
		}
		$arg = '?'.implode('&', $parametros);
		$this->rutas['users'] = '/api/users'.$arg;
		$this->rutas['lrdd'] = '/.well-known/webfinger?resource='.$id;
		if(filter_var($id, FILTER_VALIDATE_URL)) {
			$id = $this->objeto($id);
			$rutas = array(
				'object' => '/api/'.$id['tipo'].'/'.$id['idlocal'],
				'replies' => '/api/'.$id['tipo'].'/'.$id['idlocal'].'/replies'.$arg,
				'likes' => '/api/'.$id['tipo'].'/'.$id['idlocal'].'/likes'.$arg,
				'shares' => '/api/'.$id['tipo'].'/'.$id['idlocal'].'/shares'.$arg
			);
		} else {
			$rutas = array(
				'outbox' => '/api/user/'.$id.'/feed'.$arg,
				'outbox minor' => '/api/user/'.$id.'/feed/minor'.$arg,
				'outbox major' => '/api/user/'.$id.'/feed/major'.$arg,
				'inbox' => '/api/user/'.$id.'/inbox'.$arg,
				'inbox minor' => '/api/user/'.$id.'/inbox/minor'.$arg,
				'inbox major' => '/api/user/'.$id.'/inbox/major'.$arg,
				'inbox direct' => '/api/user/'.$id.'/inbox/direct'.$arg,
				'inbox direct minor' => '/api/user/'.$id.'/inbox/direct/minor'.$arg,
				'inbox direct major' => '/api/user/'.$id.'/inbox/direct/major'.$arg,
				'followers' => '/api/user/'.$id.'/followers'.$arg,
				'following' => '/api/user/'.$id.'/following'.$arg,
				'favorites' => '/api/user/'.$id.'/favorites'.$arg,
				'lists' => '/api/user/'.$id.'/lists/person'.$arg,
				'user' => '/api/user/'.$id,
				'profile' => '/api/user/'.$id.'/profile',
				'uploads' => '/api/user/'.$id.'/uploads',
			);
		}
		$this->rutas = array_merge($this->rutas, $rutas);
		return $this->rutas;
	}
	private function arrayAutorizacion() {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$fecha = new DateTime();
		$selloTiempo = $fecha->format("U");
		$publico = array(
			'oauth_callback' => 'oob',
			'oauth_consumer_key' => $this->consumer_key,
			'oauth_nonce' => $this->nonce(),
			'oauth_signature_method' => $this->metodoCod,
			'oauth_timestamp' => $selloTiempo,
			'oauth_version' => '1.0'
		);
		if($this->token) {
			$publico['oauth_token'] = $this->token;
		}
		if($this->cliente['redirect_uris']) {
			$publico['oauth_callback'] = $this->cliente['redirect_uris'];
		}
		$secreto = array(
			'ocs' => $this->consumer_secret,
			'ots' => $this->token_secret
		);
		return array('publico' => $publico, 'secreto' => $secreto);
	}
	public function cURL($URL, $metodo, $cabecera_extra, $post) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$ch = curl_init($URL);
		$opciones = array(
			CURLOPT_SSL_VERIFYPEER => false,
   		CURLOPT_RETURNTRANSFER => true,
   		CURLINFO_HEADER_OUT => true,
   		CURLOPT_FOLLOWLOCATION => false,
   		CURLOPT_CONNECTTIMEOUT => 10,
   		CURLOPT_HTTPHEADER => array(
   		)
		);
		if($post) {
			$opciones[CURLOPT_POSTFIELDS] = $post;
		}
		if($metodo === "POST") {
			$opciones[CURLOPT_POST] = true;
		}
		if($metodo === "PUT") {
			$opciones[CURLOPT_CUSTOMREQUEST] = 'PUT';
		}
		if($cabecera_extra) {
			$opciones[CURLOPT_HTTPHEADER] = array_merge($opciones[CURLOPT_HTTPHEADER], $cabecera_extra);
		}
		curl_setopt_array($ch, $opciones);
		$exec = curl_exec($ch);
		$curl_info = curl_getinfo($ch);
		$curl_info['error'] = curl_error($ch);
		$curl_info['errno'] = curl_errno($ch);
		$extra_info = explode(';',$curl_info['content_type']);
		$charset = array_values(preg_grep("/charset.*/", $extra_info));
		$charset = explode('=', trim($charset[0]));
		$charset = $charset[1];
		$mime = array_values(preg_grep("/^.*\/.*$/", $extra_info));
		$mime = trim($mime[0]);
		$respuesta = array();
		$respuesta['mime'] = $mime;
		$respuesta['charset'] = $charset;
		$respuesta['http_code'] = $curl_info['http_code'];
		$respuesta['errno'] = curl_errno($ch);
		$respuesta['error'] = curl_error($ch);
		$respuesta['all'] = $curl_info;
		if($exec){
			$respuesta['content'] = $exec;
		}
		curl_close($ch);
		$this->registro[] = ["return" => ["method" => __FUNCTION__, "return" => $respuesta]];
		return $respuesta;
	}
	function registrarCliente($nodo) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$URL = 'https://' . $nodo . $this->rutas['rc'];
		$metodo = 'POST';
		$cabecera_extra = array('Accept: application/json', 'Content-Type: application/json');
		$post = json_encode($this->cliente);
		$registro = $this->cURL($URL, $metodo, $cabecera_extra, $post);
		if($registro['http_code'] != 200){
			#si la conexion segura falla, se prueba con la conexión común
			$URL = 'http://' . $nodo . $this->rutas['rc'];
			$registro_inseguro = $this->cURL($URL, $metodo, $cabecera_extra, $post);
			#si aun asi la conexión falla entonces imprime un mensaje de error en pantalla, se debe redirigir a otro sitio!!!!
			if($registro_inseguro['http_code'] != 200){
				return false;
			}
			else {
				$conexion = '';
				$credenciales = json_decode($registro_inseguro['content'], true);
			}
		}
		else {
			$conexion = 's';
			$credenciales = json_decode($registro['content'], true);
		}
		$oauth_consumer_key = $credenciales["client_id"];
		$oauth_consumer_secret = $credenciales["client_secret"];
		$resultado = array(
			'conexion' => $conexion,
			'consumer_key' => $oauth_consumer_key,
			'consumer_secret' => $oauth_consumer_secret
		);
		return $resultado;
	}
	private function nonce() {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$mt = explode(' ', microtime());
		return $mt[1] . substr($mt[0], 2, 6);
	}
	private function authorizationOauth(array $publico, array $secreto, $metodo, $enlace) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$parametros = "";
		$authorization = "";
		#buscamos, si es que los hay, argumentos extras en al url y las agregamos a las variables para la firma.
		$argumentosURL = array();
		$query = parse_url($enlace, PHP_URL_QUERY);
		$enlace = str_replace('?'.$query, '', $enlace);#quitamos los argumentos y el simbolo "?"
		parse_str($query, $argumentosURL);
		$a_param = array_merge($publico, $argumentosURL);
		ksort($a_param);
		foreach ($a_param as $c => $v) {
			$parametros.='&'.$c.'='.rawurlencode($v);
		}
		$parametros = substr($parametros, 1, strlen($parametros));#quita el ampersand del principio de la cadena
		#Inicio codificación firma
		$parametros = rawurlencode($parametros);
		$base_firma = $metodo.'&'.rawurlencode($enlace).'&'.$parametros;
		$llave_cod = rawurlencode($secreto["ocs"]).'&'.rawurlencode($secreto["ots"]);
		$firma = hash_hmac('SHA1',$base_firma,$llave_cod,true);
		$firma = base64_encode($firma);
		#fin codificacion firma
		$publico['oauth_signature'] = $firma;
		ksort($publico);
		foreach ($publico as $c => $v) {
			$authorization.=','.$c.'="'.rawurlencode($v).'"';
		}
		$authorization = substr($authorization, 1, strlen($authorization));#quita la primera coma
		return $authorization;
	}
	public function pumpId($pumpid, $peticion) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
			if($peticion == "nodo") {
				$nodo = substr($pumpid, stripos($pumpid,'@')+1, strlen($pumpid));
				return $nodo;
			}
			elseif($peticion == "alias") {
				$alias = substr($pumpid, 0,stripos($pumpid,'@'));
				return $alias;
			}
			else {
				return FALSE;
			}
	}
	function tokenTmp() {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$metodo_http = 'POST';
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$this->rutas['tt'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cuerpo = "";
		$cabecera = array('Authorization: OAuth '.$authorization);
		$tokentmp_post = $this->cURL($enlace, $metodo_http, $cabecera, $cuerpo);
		if($tokentmp_post['http_code'] != 200) {
			$authorization = $this->authorizationOauth($publico, $secreto, 'GET', $enlace);
			$cabecera = array('Authorization: OAuth '.$authorization);
			$tokentmp_get = $this->cURL($enlace, $metodo_http, 'GET', $cuerpo);
			if($tokentmp_get['http_code'] != 200) {
				$authorization = str_replace('"', '', $authorization);
				$authorization = str_replace(',', '&', $authorization);
				$cabecera = '';
				$tokentmp_get2 = $this->cURL($enlace.'?'.$authorization, "GET", $cabecera, $cuerpo);
				if($tokentmp_get2['http_code'] != 200) {
					return false;
				}
				else {
					$parse_url = 'http://siteweb.com/lol?'.$tokentmp_get2['content'];
					$query = parse_url($parse_url, PHP_URL_QUERY);
					$credenciales_tmp = array();
					parse_str($query, $credenciales_tmp);
					$credenciales_tmp['url'] = 'http'.$this->conexion.'://'.$this->nodo.$this->rutas['aut'].$credenciales_tmp['oauth_token'];
					return $credenciales_tmp;
				}
			}
			else {
				$parse_url = 'http://siteweb.com/lol?'.$tokentmp_get['content'];
				$query = parse_url($parse_url, PHP_URL_QUERY);
				$credenciales_tmp = array();
				parse_str($query, $credenciales_tmp);
				$credenciales_tmp['url'] = 'http'.$this->conexion.'://'.$this->nodo.$this->rutas['aut'].$credenciales_tmp['oauth_token'];
				return $credenciales_tmp;
			}
		}
		else {
			$parse_url = 'http://siteweb.com/lol?'.$tokentmp_post['content'];
			$query = parse_url($parse_url, PHP_URL_QUERY);
			$credenciales_tmp = array();
			parse_str($query, $credenciales_tmp);
			$credenciales_tmp['url'] = 'http'.$this->conexion.'://'.$this->nodo.$this->rutas['aut'].$credenciales_tmp['oauth_token'];
			return $credenciales_tmp;
		}
	}
	function token($oauth_token, $oauth_token_secret, $oauth_verifier) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$metodo_http = 'POST';
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$this->rutas['ta'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$publico['oauth_token'] = $oauth_token;
		$publico['oauth_verifier'] = $oauth_verifier;
		$secreto = $arrayAutorizacion['secreto'];
		$secreto['ots'] = $oauth_token_secret;
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cuerpo = "";
		$cabecera = array('Authorization: OAuth '.$authorization);
		$tokenacc_post = $this->cURL($enlace, $metodo_http, $cabecera, $cuerpo);
		if($tokenacc_post['http_code'] != 200) {
			$authorization = $this->authorizationOauth($publico, $secreto, 'GET', $enlace);
			$cabecera = array('Authorization: OAuth '.$authorization);
			$tokenacc_get = $this->cURL($enlace, 'GET', $cabecera, $cuerpo);
			if($tokenacc_get['http_code'] != 200) {
				return false;
			}
			else {
				$parse_url = 'http://siteweb.com/lol?'.$tokenacc_get['content'];
				$query = parse_url($parse_url, PHP_URL_QUERY);
				$credenciales_acc = array();
				parse_str($query, $credenciales_acc);
				return $credenciales_acc;
			}
		}
		else {
			$parse_url = 'http://siteweb.com/lol?'.$tokenacc_post['content'];
			$query = parse_url($parse_url, PHP_URL_QUERY);
			$credenciales_acc = array();
			parse_str($query, $credenciales_acc);
			return $credenciales_acc;
		}
		
	}
	private function publicar($posteo) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$ruta = $this->rutasGet($this->alias, false, false, false, false, false);
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$ruta['outbox'];
		$metodo_http = 'POST';
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Content-Type: application/json', 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, json_encode($posteo));
		if($response['http_code'] == 200) {
			return json_decode($response['content'],true);
		}
		else {
			return false;
		}
	}
	private function actualizar(array $posteo, $id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$metodo_http = 'PUT';
		$id = $this->objeto($id);
		$ruta = $this->rutasGet(null, null, null, null, $id['tipo'], $id['idlocal']);
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$ruta['object'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Content-Type: application/json', 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, json_encode($posteo));
		if($response['http_code'] == 200) {
			return json_decode($response['content'],true);
		}
		else {
			return false;
		}
	}
	private function cargarArchivo($archivo, $mime) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$metodo_http = 'POST';
		$ruta = $this->rutasGet($this->alias, false, false, false, false, false);
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$ruta['uploads'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Content-Type: '.$mime, 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, $archivo);
		if($response['http_code'] == 200) {
			return json_decode($response['content'], true);
		}
		else {
			return false;
		}
	}
	function archivo($archivo, PumpJSON $pumpjson) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		#una falla o algo hace q el archivo cargado y despues publicado su contenido no aparesca,
		#por lo que hay que actualizar el contenido para que aparesca el cuerpo de la publicacion.
		if(strpos($archivo, 'http://') === 0 || strpos($archivo, 'https://') === 0) {
			$archivo = file_get_contents($archivo);
		}
		else {
			$archivo = file_get_contents(realpath($archivo));
		}
		$file = new finfo(FILEINFO_MIME);
		$mime = explode(';', $file->buffer($archivo));
		$mime = $mime[0];
		$upload = $this->cargarArchivo($archivo, $mime);
		$pumpjson->verb = 'post';
		$pumpjson->object['id'] = $upload['id'];
		$pumpjson->object['objectType'] = $upload['objectType'];
		$post = array(
			'verb' => $pumpjson->verb,
			'object' => $pumpjson->object,
		);
		if(!is_null($pumpjson->to)) {
			$post['to'] = $pumpjson->to;
		}
		if(!is_null($pumpjson->cc)) {
			$post['cc'] = $pumpjson->cc;
		}
		if(!is_null($pumpjson->bto)) {
			$post['bto'] = $pumpjson->bto;
		}
		if(!is_null($pumpjson->bcc)) {
			$post['bcc'] = $pumpjson->bcc;
		}
		$object = $this->publicar($post);
		$id = $object['object']['id'];
		return $this->actualizar($post['object'], $id);
	}
	function nota(PumpJSON $pumpjson) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$pumpjson->object['objectType'] = 'note';
		$post = array(
			'verb' => $pumpjson->verb,
			'object' => $pumpjson->object,
		);
		if(!is_null($pumpjson->to)) {
			$post['to'] = $pumpjson->to;
		}
		if(!is_null($pumpjson->cc)) {
			$post['cc'] = $pumpjson->cc;
		}
		if(!is_null($pumpjson->bto)) {
			$post['bto'] = $pumpjson->bto;
		}
		if(!is_null($pumpjson->bcc)) {
			$post['bcc'] = $pumpjson->bcc;
		}
		return $this->publicar($post);
	}
	function editar($id, $contenido, $titulo) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$post['content'] = $contenido;
		if($titulo) {
			$post['displayName'] = $titulo;
		}
		return $this->actualizar($post, $id);
	}
	function solicitud($peticion, $id, ...$cyp) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if($id == 'yo') {
			$ruta = $this->rutasGet($this->alias, ...$cyp);
		} else {
			$ruta = $this->rutasGet($id, ...$cyp);
		}
		$ruta = $ruta[$peticion];
		$enlace = 'http'.$this->conexion.'://'.$this->nodo. $ruta;
		$metodo_http = 'GET';
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Cache-Control: no-cache', 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, '');
		if($response['http_code'] == 200) {
			return json_decode($response['content'], true);
		}
		else {
			return false;
		}
	}
	function gJson($URL) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$curl = $this->cURL($URL, 'GET', false, false);
		if($curl['http_code'] != 200) {
			return false;
		}
		return json_decode($curl['content'], true);
	}
	function gJson_esp($URL) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$curl = $this->cURL($URL, 'GET', 0, 0);
		return json_decode(strip_tags($curl['content']), true);
	}
	function objeto($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$realid = $id;
		$id = str_replace('http://', '', $id);
		$id = str_replace('https://', '', $id);
		$id = str_replace('/api/', '/', $id);
		$id = explode('/', $id);
		return array('nodo' => $id[0], 'tipo' => $id[1], 'idlocal' => $id[2], 'id' => $realid);
	}
	function pingFirehose($url, $json) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$cabecera = array('Content-Type: application/json');
		$response = $this->cURL($url, "POST", $cabecera, $json);
		return $response;
	}
	function seguir($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if(strpos($id, 'acct:') === false) {
			$id = 'acct:'.$id;
		}
		$post['verb'] = 'follow';
		$post['object']['objectType'] = 'person';
		$post['object']['id'] = $id;
		return $this->publicar($post);
	}
	function dejardeseguir($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$post['verb'] = 'stop-following';
		$post['object']['objectType'] = 'person';
		$post['object']['id'] = 'acct:'.$id;
		return $this->publicar($post);
	}
	function megusta($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$tipo = $this->objeto($id);
		$post['verb'] = 'like';
		$post['object']['objectType'] = $tipo['tipo'];
		$post['object']['id'] = $id;
		return $this->publicar($post);
	}
	function yanomegusta($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$tipo = $this->objeto($id);
		$post['verb'] = 'unlike';
		$post['object']['objectType'] = $tipo['tipo'];
		$post['object']['id'] = $id;
		return $this->publicar($post);
	}
	function compartir($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$tipo = $this->objeto($id);
		$post['verb'] = 'share';
		$post['object']['objectType'] = $tipo['tipo'];
		$post['object']['id'] = $id;
		return $this->publicar($post);
	}
	function eliminar($id) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$tipo = $this->objeto($id);
		$post['verb'] = 'delete';
		$post['object']['objectType'] = $tipo['tipo'];
		$post['object']['id'] = $id;
		return $this->publicar($post);
	}
	function comentar($id, $comentario) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$tipo = $this->objeto($id);
		$post['verb'] = 'post';
		$post['object']['objectType'] = 'comment';
		$post['object']['content'] = $comentario;
		$post['object']['inReplyTo']['id'] = $id;
		$post['object']['inReplyTo']['objectType'] = $tipo['tipo'];
		return $this->publicar($post);
	}
	function editarPerfil($nombre, $bio, $lugar) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if($nombre) {
			$put['displayName'] = $nombre;
		}
		if($bio) {
			$put['summary'] = $bio;
		}
		if($lugar) {
			$put['location']['displayName'] = $lugar;
		}
		$metodo_http = 'PUT';
		$ruta = $this->rutasGet($this->alias, null, null, null, null, null);
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$ruta['profile'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Content-Type: application/json', 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, json_encode($put));
		if($response['http_code'] == 200) {
			return json_decode($response['content'],true);
		}
		else {
			return false;
		}
	}
	function editarCuenta($contrasena, $email) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		$put['nickname'] = $this->alias;
		if($contrasena) {
			$put['password'] = $contrasena;
		}
		if($email) {
			$put['email'] = $email;
		}
		$metodo_http = 'PUT';
		$ruta = $this->rutasGet($this->alias, null, null, null, null, null);
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$ruta['user'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Content-Type: application/json', 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, json_encode($put));
		if($response['http_code'] == 200) {
			return json_decode($response['content'],true);
		}
		else {
			return false;
		}
	}
	function registrarUsuario($alias, $contrasena) {
		$this->registro[] = ["call" => ["method" => __FUNCTION__, "var" => get_defined_vars()]];
		if(!$alias || !$contrasena) {
			return false;
		}
		$usuario['nickname'] = $alias;
		$usuario['password'] = $contrasena;
		$metodo_http = 'POST';
		$ruta = $this->rutasGet(null, null, null, null, null, null);
		$enlace = 'http'.$this->conexion.'://'.$this->nodo.$ruta['users'];
		$arrayAutorizacion = $this->arrayAutorizacion();
		$publico = $arrayAutorizacion['publico'];
		$secreto = $arrayAutorizacion['secreto'];
		$authorization = $this->authorizationOauth($publico, $secreto, $metodo_http, $enlace);
		$cabecera = array('Accept: application/json', 'Content-Type: application/json', 'Authorization: OAuth '.$authorization);
		$response = $this->cURL($enlace, $metodo_http, $cabecera, json_encode($usuario));
		if($response['http_code'] == 200) {
			return json_decode($response['content'],true);
		}
		else {
			return false;
		}
	}
	function __destruct() {
		if($this->nvlreg > 0) {
			echo "<div class=\"cpregistro\"><pre>";
			print_r($this->registro);
			echo "</pre></div>";
		}
	}
}

class PumpJSON {
	private $cliente = null;
	private $usuario = null;
	private $alias = null;
	private $nodo = null;
	private $conexion = null;
	function __construct(ConectorPump $conectorpump) {
		$this->cliente = $conectorpump->cliente;
		$this->usuario = $conectorpump->usuario;
		$this->alias = $conectorpump->alias;
		$this->nodo = $conectorpump->nodo;
		$this->conexion = $conectorpump->conexion;
	}
	public $object = null;
	public $verb = null;
	public $to = null;
	public $cc = null;
	public $bto = null;
	public $bcc = null;
	function objeto($content, $name, $id) {
		$object = array(
			'content' => $content
		);
		if(!is_null($name)) {
			$object['displayName'] = $name;
		}
		if(!is_null($id)) {
			$object['id'] = $id;
		}
		$this->object = $object;
	}
	function destinatarios(array $to, array $cc, array $bto, array $bcc) {
		if(!is_null($to) || count($to)>0) {
			$this->to = $this->generarDestinatario('to', $to);
		}
		if(!is_null($bto) || count($bto)>0) {
			$this->bto = $this->generarDestinatario('bto', $bto);
		}
		if(!is_null($cc) || count($cc)>0) {
			$this->cc = $this->generarDestinatario('cc', $cc);
		}
		if(!is_null($bcc) || count($bcc)>0) {
			$this->bcc = $this->generarDestinatario('bcc', $bcc);
		}
	}
	function generarDestinatario($type, array $id) {
		$post = array();
		for($x=0;$x<count($id);$x++) {
			switch($id[$x]) {
				case 'followers': $post[$type][$x]['objectType'] = 'collection';
										$post[$type][$x]['id'] = 'http'.$this->conexion.'://'.$this->nodo.'/api/user/'.$this->alias.'/followers';
										break;
				case 'public':		$post[$type][$x]['objectType'] = 'collection';
										$post[$type][$x]['id'] = 'http://activityschema.org/collection/public';
										break;
				case (preg_match("/.*@.{3,}/", $id[$x]) ? true : false):	$post[$type][$x]['objectType'] = 'person';
																								$post[$type][$x]['id'] = 'acct:'.$id[$x];
																								break;
			}
		}
		return $post[$type];
	}
}
class unaLinea extends ConectorPump {
	#Metodos estaticos y publicos, para ejecutar acciones en una sola linea.
	#Statics and public methods, for do actions with one line.
}
?>